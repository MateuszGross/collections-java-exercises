import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.LinkedList;
import  printingLists.PrintArrayList;
import printingLists.PrintLinkedList;
import printingLists.PrintListByIterator;

/*
*    Create ArrayList and LinkedList in main()
*    Add in created lists some elements
*    Create class which will be responsible for printing ArrayList
*    Create class which will be responsible for printing LinkedList
*    Create class which will be responsible for printing List by ListIterator
*    In main() execute printing you've created
*    In main() remove some elements from list
*    In main() print again and check if the result is as expected.
*
* */
public class Main {

    public static void main(String[] args) {
        System.out.println("List exercise");
        ArrayList<String> arrayList1 = new ArrayList<>();
        LinkedList<String> linkedList1 = new LinkedList<>();
        arrayList1.add("1");
        arrayList1.add("2");
        arrayList1.add("3");
        linkedList1.add("7");
        linkedList1.add("8");
        linkedList1.add("9");

        System.out.println("Printed array list: ");
        PrintArrayList.PrintList(arrayList1);
        System.out.println("Printed linked list: ");
        PrintLinkedList.PrintList(linkedList1);

        arrayList1.remove(0);
        linkedList1.remove(1);

        System.out.println("Printed array list by iterator: ");
        PrintListByIterator.PrintArrayListByIterator(arrayList1);
        System.out.println("Printed linked list by iterator: ");
        PrintListByIterator.PrintLinkedListByIterator(linkedList1);


    }
}