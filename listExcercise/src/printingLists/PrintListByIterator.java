package printingLists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class PrintListByIterator {


    public static void PrintArrayListByIterator(ArrayList list) {
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public static void PrintLinkedListByIterator(LinkedList list) {
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
