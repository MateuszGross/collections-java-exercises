import animal.Animal;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
 *  - Use IntStream and get average of age
 *  - Use DoubleStream and get statistics about weight
 *
 *  useful: mapToInt, mapToDouble, average, summaryStatistics
 * */


public class H_SpecialStreamsExercise {
    public static void main(String[] args) {
        List<Animal> animals = Arrays.asList(
                new Animal("Elephant", 20, 6000.0),
                new Animal("Horse", 8, 800.0),
                new Animal("Dog", 5, 30.0),
                new Animal("Cat", 7, 10.0),
                new Animal("Cat", 7, 10.0)
        );

        double averageAge = animals.stream()
                .mapToInt(animal -> animal.getAge())
                .average()
                .orElseThrow( ()-> new RuntimeException());

        System.out.println("Averge age of animal is " + averageAge);

        DoubleSummaryStatistics statisticOfAnimalWeight = animals.stream()
                .mapToDouble(animal -> animal.getWeight())
                .summaryStatistics();



        System.out.println("Statistic of animal weight" );
        System.out.println(statisticOfAnimalWeight);
    }
}
