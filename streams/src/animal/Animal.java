package animal;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Animal {
    private final String name;
    private int age;
    private final double weight;
    private List<Animal> children;

    public Animal(String name, int age, double weight) {
        this(name, age, weight, Collections.emptyList());
    }

    public Animal(String name, int age, double weight, List<Animal> children) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getWeight() {
        return weight;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Animal> getChildren() {
        return children;
    }

    public void setChildren(List<Animal> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return age == animal.age &&
                Double.compare(animal.weight, weight) == 0 &&
                Objects.equals(name, animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, weight);
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                '}';
    }
}
