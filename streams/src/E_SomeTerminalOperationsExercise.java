import animal.Animal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 *  - check if there is animal with weight > 5000
 *  - check if there is no animal with age > 20
 *  - check if there any animal with weight > 1000.0
 *  - count the number of animals with age < 20
 *
 *  useful: .stream(), anyMatch(), noneMatch(), filter(), count()
 * */


public class E_SomeTerminalOperationsExercise {
    public static void main(String[] args) {
        List<Animal> animals = Arrays.asList(
                new Animal("Elephant", 20, 6000.0),
                new Animal("Horse", 8, 800.0),
                new Animal("Dog", 5, 30.0),
                new Animal("Cat", 7, 10.0),
                new Animal("Cat", 7, 10.0)
        );

        boolean anyWithWeightGreaterThen5000kg = animals.stream()
                .anyMatch(animal -> animal.getWeight() > 5000);
        System.out.println("there is animal with weight > 5000: " + anyWithWeightGreaterThen5000kg);

        boolean noAnimalWithAgeGreaterThen20 = animals.stream()
                .noneMatch(animal -> animal.getAge() > 20);
        System.out.println("there is no animal with age > 20: " + noAnimalWithAgeGreaterThen20);

        boolean anyWithWeightGreaterThen1000kg = animals.stream()
                .anyMatch(animal -> animal.getWeight() > 1000);
        System.out.println("there is animal with weight > 1000: " + anyWithWeightGreaterThen1000kg);

        long count = animals.stream()
                .filter(animal -> animal.getAge() < 20)
                .count();
        System.out.println("Number of animals withage < 20: " + count);

    }
}
