import animal.Animal;

import java.util.Arrays;
import java.util.List;

/*
 *  - Use distinct() function and print only unique animals
 *  - Print last 3 animals
 *  - Change the age of animals
 *
 *  useful: stream, distinct, skip, peek, foreach
 *
 * */


public class G_DistinctPeekSkipExercise {
    public static void main(String[] args) {
        List<Animal> animals = Arrays.asList(
                new Animal("Elephant", 20, 6000.0),
                new Animal("Horse", 8, 800.0),
                new Animal("Dog", 5, 30.0),
                new Animal("Cat", 7, 10.0),
                new Animal("Cat", 7, 10.0)
        );

        animals.stream()
                .distinct()
                .forEach(animal -> System.out.println(animal));

        System.out.println(" ");
        System.out.println("Print last 3 animals");
        animals.stream()
                .skip(2)
                .forEach(animal -> System.out.println(animal));

        System.out.println("");
        animals.stream()
                //.filter(animal -> animal.getAge() < 8)
                .peek(animal -> animal.setAge(3))
                .forEach(animal -> System.out.println(animal));




    }
}
