import animal.Animal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
 *
 *  - filter animals with children and collect to list
 *  - filter animals with children and collect children to list
 *
 * useful: stream(), filter(), collect(Collector.toList())
 *
 * */


public class C_CollectorExercise {
    public static void main(String[] args) {
        List<Animal> elephantChildren = Arrays.asList(new Animal("Small Elephant", 20, 4000.0), new Animal("Small Elephant", 20, 3500));
        List<Animal> horseChildren = Arrays.asList(new Animal("Small Horse", 4, 800.0));

        List<Animal> animals = Arrays.asList(
                new Animal("Elephant", 20, 6000.0, elephantChildren),
                new Animal("Horse", 15, 800.0, horseChildren),
                new Animal("Dog", 5, 30.0),
                new Animal("Cat", 7, 10.0),
                new Animal("Cat", 7, 10.0)
        );
        List<Animal> animalsWithChildren = animals.stream()
                .filter(a-> !a.getChildren().isEmpty())
                .collect(Collectors.toList());
        System.out.println("Animal With Children " + animalsWithChildren);
        List<Animal> animalsChildren = animals.stream()
                .filter(a-> !a.getChildren().isEmpty())
                .map(a-> a.getChildren())
                .flatMap(a-> a.stream())
                .collect(Collectors.toList());
        System.out.println("Animal Children " + animalsChildren);

    }
}
