import animal.Animal;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/*
 *  - Find animal with max age
 *  - Find animal with minimum age
 *  - Filter animals with weight < 2000 and sum their age (with reduce function)
 *
 *  useful: stream, max, min, filter(), reduce()
 * */


public class F_MaxAndMinimumExercise {
    public static void main(String[] args) {
        List<Animal> animals = Arrays.asList(
                new Animal("Elephant", 20, 6000.0),
                new Animal("Horse", 8, 800.0),
                new Animal("Dog", 5, 30.0),
                new Animal("Cat", 6, 10.0)
        );
        Animal animalWithMaxAge = animals.stream()
                .max(Comparator.comparing(Animal::getAge))
                .orElseThrow(()-> new RuntimeException());
        System.out.println("Animal with max age: " + animalWithMaxAge.getName());


        Animal animalWithMinAge = animals.stream()
                .min(Comparator.comparing(Animal::getAge))
                .orElseThrow( ()-> new RuntimeException());
        System.out.println("Animal with min age: " + animalWithMinAge.getName());


        int sumOfAgeAnimalsWithWeightLessThen2000 = animals.stream()
                .filter(animal -> animal.getWeight() < 2000)
                .map(animal -> animal.getAge())
                .reduce(0, (a, b) -> a + b);
        System.out.println("Sum of age animals with weight less then 2000: " + sumOfAgeAnimalsWithWeightLessThen2000);
    }
}
