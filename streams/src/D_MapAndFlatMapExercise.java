import animal.Animal;

import java.util.Comparator;
import java.util.Arrays;
import java.util.List;

/*
 *  - print only names of animals use map
 *  - filter animals with children and print their names (parent names)
 *  - filter animals with children and print children names
 *  - find the max age of children
 *  - sum the children age with reduce
 *
 *  useful: stream(), filter(), list.isEmpty(), map(), flatMap(), max(), reduce()
 * */


public class D_MapAndFlatMapExercise {
    public static void main(String[] args) {
        List<Animal> elephantChildren = Arrays.asList(new Animal("Small Elephant", 20, 4000.0), new Animal("Small Elephant", 20, 3500));
        List<Animal> horseChildren = Arrays.asList(new Animal("Small Horse", 4, 800.0));

        List<Animal> animals = Arrays.asList(
                new Animal("Elephant", 20, 6000.0, elephantChildren),
                new Animal("Elephant", 50, 6000.0, elephantChildren),
                new Animal("Horse", 15, 800.0, horseChildren),
                new Animal("Dog", 5, 30.0),
                new Animal("Cat", 7, 10.0),
                new Animal("Cat", 7, 10.0)
        );
        animals.stream()
                .map(a -> a.getName())
                .forEach(a -> System.out.println(a));
        //animals.stream()
               //.forEach(a -> System.out.println(a.getChildren()));
        System.out.println(" ");
        System.out.println("Animals with Children");
        animals.stream()
                .filter(a-> !a.getChildren().isEmpty())
                .map(a-> a.getName())
                .forEach(a-> System.out.println(a));
                //.map(a -> a.getChildren())
        System.out.println(" ");
        System.out.println("Names of animals children:");
        animals.stream()
                .filter(a-> !a.getChildren().isEmpty())
                .map(a-> a.getChildren())
                .flatMap(a-> a.stream())
                .forEach(a-> System.out.println(a.getName()));
        System.out.println(" ");

        System.out.println("Maximal age of animals children: ");
        int maxAge= animals.stream()
                .filter(a-> !a.getChildren().isEmpty())
                .map(a-> a.getChildren())
                .flatMap(a-> a.stream())
                .max(Comparator.comparing(Animal::getAge))
                .map(a -> a.getAge())
                .orElseThrow( ()-> new RuntimeException());
        System.out.println(maxAge);

        System.out.println("");
        System.out.println("Sum of anmials children age:");

        int sumOfAge= animals.stream()
                .filter(a-> !a.getChildren().isEmpty())
                .map(a-> a.getChildren())
                .flatMap(a-> a.stream())
                .map(a -> a.getAge())
                .reduce(0, (a, b) -> a + b);
        System.out.println(sumOfAge);
    }
}
