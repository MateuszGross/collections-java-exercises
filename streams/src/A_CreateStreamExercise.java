import animal.Animal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

/*
*
*   Create stream from list of Animals (you have to create list of animals first)
*   Create stream using java.util.stream.Stream
*   Create stream of int with help of IntStream
*   Create stream from array
*
*   Useful: .stream(), Stream.of, Arrays.stream
*
* */
import animal.Animal;

public class A_CreateStreamExercise {

    public static void main(String[] args) {
        ArrayList<Animal> animalsList = new ArrayList<>();
        animalsList.add(new Animal("Dog", 3, 20.6));
        animalsList.add(new Animal( "Rat",1,0.5));
        animalsList.add(new Animal("Cow", 5, 320));
        animalsList.add(new Animal("Bat",2,0.07));
    animalsList.stream()
            .filter(animal->animal.getAge() > 2)
            .forEach(animal->System.out.println(animal));

        IntStream.range(1,5)
                .forEach(a->System.out.println(a));
    }
}
