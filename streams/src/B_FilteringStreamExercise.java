import animal.Animal;
import java.util.stream.IntStream;
import java.util.Arrays;
import java.util.List;

/*
*   Filter animals with age more than 7 and print
*   Filter animals with weight more than 30.0 and print
*
*   useful: .stream() .filter() .forEach()
* */
public class B_FilteringStreamExercise {

    public static void main(String[] args) {
        List<Animal> animals = Arrays.asList(
                new Animal("Elephant", 20, 6000.0),
                new Animal("Horse", 8, 800.0),
                new Animal("Dog", 5, 30.0),
                new Animal("Cat", 7, 10.0)
        );
        animals.stream()
                .filter(a -> a.getAge()>7)
                .forEach(a -> System.out.println(a));
        animals.stream()
                .filter(a-> a.getWeight()>30.0)
                .forEach(a -> System.out.println(a));






    }
}
