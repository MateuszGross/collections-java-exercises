import java.util.*;

/*
 *   - put cars per given brand to carsNumberPerBrand map
 *   - create a method which will print map
 *   - create a method which will find the largest car number per brand and return brand
 *   - create a map which will sort elements by alphabetic order and print it
 *
 * */
public class MapExercise {

    public static void main(String[] args) {
        Map<String, Integer> carsNumberPerBrand = new HashMap<>();
        carsNumberPerBrand.put("Audi", 5);
        carsNumberPerBrand.put("Kia", 1);
        carsNumberPerBrand.put("BMW", 2);
        carsNumberPerBrand.put("Mercedes", 3);
        carsNumberPerBrand.put("Subaru", 4);

        Map<String, String> sortin = new TreeMap<>();
        sortin.put("Alex", "");
        sortin.put("Leja", "");
        sortin.put("Zygfrid", "");
        sortin.put("Viene", "");
        sortin.put("Bark", "");

        System.out.println("Alphabetic order: ");
        sortin.forEach((k,v)->System.out.println(k));
        System.out.println();

        carsNumberPerBrand.forEach((k,v)->System.out.println("Car : " + k + ", Brand : " + v));

        System.out.println();

        Map.Entry<String, Integer> max = carsNumberPerBrand
                .entrySet()
                .stream()
                .max(
                        // It generates
                        // Map.Entry comparators by delegating to another
                        // comparator, exists also for keys
                        Map.Entry.comparingByValue(Integer::compareTo)
                )
                // Get the optional (optional because the map can be empty)
                .get();
        System.out.println("Max is " + max);

        }

    }
