import cars.Car;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/*
 *   - put cars to 'cars' set
 *   - print them
 *   - put at least two the same cars
 *   - print set
 *   - notice that even they are the same still you have them, that's way implement hashcode equals methods in Car.java class
 *   - print the set again
 *   - create new TreeSet and fill with new values and then print it
 *   - if you did not pass a comparator to TreeSet you have an error thats because Car do not implement
 *   Comparable interface implement that and run again
 *
 * */
public class SetExercise {

    public static void main(String[] args) {
        Set<Car> cars = new HashSet<>();
        cars.add(new Car("Audi A4", "Diesel"));

        cars.add(new Car("Audi A4", "Diesel"));
        cars.add(new Car("Toyota Corolla", "Petrol"));
        cars.add(new Car("Kia ceed", "Petrol"));

        cars.stream()
                .forEach(System.out::println);

        System.out.println();
        System.out.println("TreeSet");
        System.out.println();

        Set<Car> cars1 = new TreeSet<>();
        cars1.add(new Car("BMW X6", "Diesel"));
        cars1.add(new Car("Subaru STI", "Petrol"));
        cars1.add(new Car("Hyundai i30", "Petrol"));

        cars1.stream()
                .forEach(System.out::println);
    }
}
