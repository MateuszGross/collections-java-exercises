package student;

import student.Student;

import java.util.Comparator;
public class CompareByScore implements Comparator<Student> {
    @Override
    public int compare(Student student1, Student student2){
        return student2.getScore().compareTo(student1.getScore());
    }
}
