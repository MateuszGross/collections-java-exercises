package student;

public class Student implements Comparable<Student> {
    private String firstName;
    private String lastName;
    private Double score;
    public Student(String firstName,String lastName, Double score){
        this.firstName = firstName;
        this.lastName = lastName;
        this.score = score;
    }

    public String getFirstName(){
        return this.firstName;
    }

    public String getLastName(){
        return  this.lastName;
    }

    public Double getScore(){
        return  this.score;
    }

    @Override
    public String toString() {

        return " "+firstName+" "+lastName+" "+score;
    }

    @Override
    public int compareTo(Student student1){
        return this.getLastName().compareTo(student1.getLastName());
    }
}
