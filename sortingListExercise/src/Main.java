/*
*   Create class Student with field: firstName, lastName, score. (Score should be of type 'Double')
*   In main() create List of students with at least 3 elements
*   In main() print a list
*   Implement Comparable interface for Student class to be able compare by lastName
*   In main() execute Collections.sort() on created list
*   In main() print a list again to see the result
*   Create comparator class which will allow to sort list by score
*   In main() execute Collections.sort on created list with created comparator
*   In main() print a list again to see the result
*   In main() execute Collections.sort on created list with anonymous comparator which will compare by firstName
*   In main() print a list again to see the result
 * */
import student.CompareByScore;
import student.Student;
import java.lang.reflect.Array;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;



public class Main {

    public static void main(String[] args) {
        List<Student> students = new LinkedList<>();
        students.add(new Student("Wojciech","Da",2.00));
        students.add(new Student("Karol","C", 4.56));
        students.add(new Student("Arkadiusz", "De", 5.00));
        System.out.println("Studenci"+ students);

        System.out.println("Sorting by lastName: ");
        Collections.sort(students);
        System.out.println("Students"+ students);
        System.out.println("Sorting by score: ");
        Collections.sort(students, new CompareByScore());
        System.out.println("Students"+ students);
        System.out.println("Sorting by firstName: ");
        students.sort(new CompareByScore());
        System.out.println("Students"+ students);

    }

}
